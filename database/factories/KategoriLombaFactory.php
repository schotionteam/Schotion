<?php

namespace Database\Factories;

use App\Models\KategoriLomba;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;

class KategoriLombaFactory extends Factory
{
    protected $model = KategoriLomba::class;

    public function definition()
    {
        $uuid = Str::substr(Uuid::uuid4()->toString(), 0, 26);

        return [
            'id_kategori_lomba' => $uuid,
            'nama' => $this->faker->word,
        ];
    }
}
