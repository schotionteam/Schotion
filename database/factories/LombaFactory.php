<?php

namespace Database\Factories;

use App\Models\KategoriLomba;
use App\Models\Lomba;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;

class LombaFactory extends Factory
{
    protected $model = Lomba::class;

    public function definition()
    {
        $uuid = Str::substr(Uuid::uuid4()->toString(), 0, 26);

        return [
            'id_lomba' => $uuid,
            'nama' => $this->faker->word,
            'deskripsi' => $this->faker->sentence,
            'tanggal_mulai' => $this->faker->date,
            'tanggal_berakhir' => $this->faker->date,
            'penyelenggara' => $this->faker->company,
            'link_gambar' => $this->faker->url,
            'id_kategori_lomba' => KategoriLomba::factory(),
        ];
    }
}
