<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Pengguna;
use App\Models\Peran;
use Illuminate\Support\Facades\Auth;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_register()
    {
        $peranAdmin = Peran::factory()->create(['nama' => 'admin']);

        $response = $this->post('/auth/register', [
            'fullname' => 'John Doe',
            'email' => 'john@example.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $response->assertRedirect('/auth/login');
        $this->assertDatabaseHas('pengguna', ['email' => 'john@example.com']);
        $this->assertEquals('admin', Pengguna::where('email', 'john@example.com')->first()->peran->nama);
    }

    public function test_login_with_valid_credentials()
    {
        $peranAdmin = Peran::factory()->create(['nama' => 'admin']);
        $pengguna = Pengguna::factory()->create([
            'nama_lengkap' => 'John Doe',
            'email' => 'john@example.com',
            'password' => bcrypt('password'), // Password hash
            'id_peran' => $peranAdmin->id_peran,
        ]);

        $response = $this->post('/auth/login', [
            'email' => 'john@example.com',
            'password' => 'password',
        ]);

        $response->assertRedirect('/');
        $this->assertTrue(Auth::check());
        $this->assertEquals('john@example.com', Auth::user()->email);
    }

    public function test_login_with_invalid_credentials()
    {
        $response = $this->post('/auth/login', [
            'email' => 'invalid@example.com',
            'password' => 'invalidpassword',
        ]);

        $response->assertSessionHasErrors(['email']);
        $this->assertFalse(Auth::check());
    }
}
