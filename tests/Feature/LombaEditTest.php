<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Lomba;
use App\Models\KategoriLomba;
use App\Models\Pengguna;
use App\Models\Peran;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Session;
use Mockery\Matcher\Closure;

class LombaEditTest extends TestCase
{
    use RefreshDatabase;


    // ----------------------------------- BASIS PATH TESTING -----------------------------------
    /** @test success edit page */
    public function testEditCompetitionSuccess()
    {
        $peranAdmin = Peran::factory()->create(['nama' => 'admin']);

        $user = Pengguna::factory()->create([
            'nama_lengkap' => 'John Doe',
            'email' => 'john@example.com',
            'password' => bcrypt('password'),
            'id_peran' => $peranAdmin->id_peran,
        ]);

        $this->actingAs($user);
        $lomba = Lomba::factory()->create();

        $response = $this->get('/admin/competition/edit/' . $lomba->id_lomba);

        $response->assertStatus(200);
        $response->assertViewHas('lomba', $lomba);
    }

    // /** @test failed edit page */
    // public function testEditCompetitionNotFound()
    // {
    //     $peranAdmin = Peran::factory()->create(['nama' => 'admin']);

    //     $user = Pengguna::factory()->create([
    //         'nama_lengkap' => 'John Doe',
    //         'email' => 'john@example.com',
    //         'password' => bcrypt('password'),
    //         'id_peran' => $peranAdmin->id_peran,
    //     ]);

    //     $this->actingAs($user);
    //     $response = $this->get('/admin/competition/edit/999');

    //     $response->assertStatus(404);
    // }

    /** @test success update data */
    public function testUpdateCompetitionSuccess()
    {
        $peranAdmin = Peran::factory()->create(['nama' => 'admin']);

        $user = Pengguna::factory()->create([
            'nama_lengkap' => 'John Doe',
            'email' => 'john@example.com',
            'password' => bcrypt('password'),
            'id_peran' => $peranAdmin->id_peran,
        ]);

        $this->actingAs($user);
        $lomba = Lomba::factory()->create();

        $data = [
            'nama' => 'Lomba B',
            'deskripsi' => 'Deskripsi Lomba B',
            'tanggal_mulai' => '2024-06-01',
            'tanggal_berakhir' => '2024-06-10',
            'penyelenggara' => 'Penyelenggara B',
            'link_gambar' => 'http://example.com/imageB.jpg',
            'id_kategori_lomba' => KategoriLomba::factory()->create()->id_kategori_lomba,
        ];

        $response = $this->put('/admin/competition/update/' . $lomba->id_lomba, $data);

        $this->assertDatabaseHas('lomba', [
            'id_lomba' => $lomba->id_lomba,
            'nama' => 'Lomba B',
            'deskripsi' => 'Deskripsi Lomba B',
            'tanggal_mulai' => '2024-06-01',
            'tanggal_berakhir' => '2024-06-10',
            'penyelenggara' => 'Penyelenggara B',
            'link_gambar' => 'http://example.com/imageB.jpg',
            'id_kategori_lomba' => $data['id_kategori_lomba'],
        ]);

        $this->assertEquals('Data lomba berhasil diperbarui.', Session::get('success'));

        $this->assertNull(Session::get('failed'));

        $response->assertStatus(302);
    }


    /** @test failed update data */
    public function testUpdateCompetitionValidationFailed()
    {
        $peranAdmin = Peran::factory()->create(['nama' => 'admin']);

        $user = Pengguna::factory()->create([
            'nama_lengkap' => 'John Doe',
            'email' => 'john@example.com',
            'password' => bcrypt('password'),
            'id_peran' => $peranAdmin->id_peran,
        ]);

        $this->actingAs($user);

        $lomba = Lomba::factory()->create();
        $data = [
            'nama' => '',
            'deskripsi' => 'Deskripsi Lomba B',
            'tanggal_mulai' => '2024-06-01',
            'tanggal_berakhir' => '2024-06-10',
            'penyelenggara' => 'Penyelenggara B',
            'link_gambar' => 'http://example.com/imageB.jpg',
            'id_kategori_lomba' => KategoriLomba::factory()->create()->id_kategori_lomba,
        ];

        $response = $this->put('/admin/competition/update/' . $lomba->id_lomba, $data);

        $response->assertRedirect('/admin/competition/edit/' . $lomba->id_lomba);
        $response->assertSessionHas('failed');
    }

    // ----------------------------------- CONDITION TESTING -----------------------------------

    /** @test kondisi lomba tidak ditemukan */
    // public function testCompetitionNotFound()
    // {
    //     $peranAdmin = Peran::factory()->create(['nama' => 'admin']);

    //     $user = Pengguna::factory()->create([
    //         'nama_lengkap' => 'John Doe',
    //         'email' => 'john@example.com',
    //         'password' => bcrypt('password'),
    //         'id_peran' => $peranAdmin->id_peran,
    //     ]);

    //     $this->actingAs($user);
    //     $response = $this->get('/admin/competition/edit/999');

    //     $response->assertStatus(404);
    // }

    /** @test empty field */
    public function testEmptyField()
    {
        $peranAdmin = Peran::factory()->create(['nama' => 'admin']);

        $user = Pengguna::factory()->create([
            'nama_lengkap' => 'John Doe',
            'email' => 'john@example.com',
            'password' => bcrypt('password'),
            'id_peran' => $peranAdmin->id_peran,
        ]);

        $this->actingAs($user);

        $lomba = Lomba::factory()->create();
        $data = [
            'nama' => '',
            'deskripsi' => 'Deskripsi Lomba B',
            'tanggal_mulai' => '2024-06-01',
            'tanggal_berakhir' => '2024-06-10',
            'penyelenggara' => 'Penyelenggara B',
            'link_gambar' => 'http://example.com/imageB.jpg',
            'id_kategori_lomba' => KategoriLomba::factory()->create()->id_kategori_lomba,
        ];

        $response = $this->put('/admin/competition/update/' . $lomba->id_lomba, $data);

        $response->assertRedirect('/admin/competition/edit/' . $lomba->id_lomba);
        $response->assertSessionHas('failed');
    }

    public function testResponseMessage()
    {
        $peranAdmin = Peran::factory()->create(['nama' => 'admin']);

        $user = Pengguna::factory()->create([
            'nama_lengkap' => 'John Doe',
            'email' => 'john@example.com',
            'password' => bcrypt('password'),
            'id_peran' => $peranAdmin->id_peran,
        ]);

        $this->actingAs($user);

        $lomba = Lomba::factory()->create();
        $data = [
            'nama' => '',
            'deskripsi' => 'Deskripsi Lomba B',
            'tanggal_mulai' => '2024-06-01',
            'tanggal_berakhir' => '2024-06-10',
            'penyelenggara' => 'Penyelenggara B',
            'link_gambar' => 'http://example.com/imageB.jpg',
            'id_kategori_lomba' => KategoriLomba::factory()->create()->id_kategori_lomba,
        ];

        $response = $this->put('/admin/competition/update/' . $lomba->id_lomba, $data);

        $response->assertRedirect('/admin/competition/edit/' . $lomba->id_lomba);
        $response->assertSessionHas('failed');

        $this->assertNotNull(session('failed'));
        $this->assertEquals('Data lomba gagal diperbarui.', session('failed'));
    }
}
